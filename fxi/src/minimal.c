/*

  GP2X minimal library v0.A by rlyeh, (c) 2005. emulnation.info@rlyeh (swap it!)

  Thanks to Squidge, Robster, snaff, Reesy and NK, for the help & previous work! :-)

  License
  =======

  Free for non-commercial projects (it would be nice receiving a mail from you).
  Other cases, ask me first.

  GamePark Holdings is not allowed to use this library and/or use parts from it.

*/

#include "minimal.h"
#include "cpuctrl.h"

static unsigned long 		gp2x_dev[3]={0,0,0};
static volatile unsigned long 	*gp2x_memregl;
volatile unsigned short 	*gp2x_memregs;
static unsigned long 		gp2x_ticks_per_second;
unsigned char 			*gp2x_screen8;
unsigned short 			*gp2x_screen15;
unsigned short 			*gp2x_logvram15[2];
unsigned long 			gp2x_physvram[2];
unsigned int			gp2x_nflip;
volatile unsigned short 	gp2x_palette[512];
volatile unsigned long 		*gp2x_2dregs;

unsigned char  			*gp2x_dualcore_ram;
unsigned long  			 gp2x_dualcore_ram_size;
unsigned char  			*gp2x_extended_ram;

void gp2x_video_flip(void)
{
  	unsigned long address=gp2x_physvram[gp2x_nflip];
	gp2x_nflip=gp2x_nflip?0:1;
  	gp2x_screen15=gp2x_logvram15[gp2x_nflip]; 
  	gp2x_screen8=(unsigned char *)gp2x_screen15; 
   	gp2x_memregs[0x290E>>1]=(unsigned short)(address & 0xFFFF);
  	gp2x_memregs[0x2910>>1]=(unsigned short)(address >> 16);
  	gp2x_memregs[0x2912>>1]=(unsigned short)(address & 0xFFFF);
  	gp2x_memregs[0x2914>>1]=(unsigned short)(address >> 16);
}


void gp2x_video_flip_single(void)
{
  	unsigned long address=gp2x_physvram[0];
  	gp2x_nflip=0;
  	gp2x_screen15=gp2x_logvram15[gp2x_nflip]; 
  	gp2x_screen8=(unsigned char *)gp2x_screen15; 
  	gp2x_memregs[0x290E>>1]=(unsigned short)(address & 0xFFFF);
  	gp2x_memregs[0x2910>>1]=(unsigned short)(address >> 16);
  	gp2x_memregs[0x2912>>1]=(unsigned short)(address & 0xFFFF);
  	gp2x_memregs[0x2914>>1]=(unsigned short)(address >> 16);
}

void gp2x_video_setgamma(unsigned short gamma) /*0..255*/
{
  	int i=256*3; 
  	gp2x_memregs[0x295C>>1]=0;                                                     
  	while(i--) gp2x_memregs[0x295E>>1]=gamma; 
}

void gp2x_video_setpalette(void)
{
  	unsigned short *g=(unsigned short *)gp2x_palette;
  	int i=512;
  	gp2x_memregs[0x2958>>1]=0;                                                     
  	while(i--) gp2x_memregs[0x295A>>1]=*g++; 
}


int sound_vol;

unsigned long gp2x_joystick_read(void)
{
  unsigned long value=(gp2x_memregs[0x1198>>1] & 0x00FF);

  if(value==0xFD) value=0xFA;
  if(value==0xF7) value=0xEB;
  if(value==0xDF) value=0xAF;
  if(value==0x7F) value=0xBE;
 
  return ~((gp2x_memregs[0x1184>>1] & 0xFF00) | value | (gp2x_memregs[0x1186>>1] << 16));
/*
  	extern int gp2x_rotate;
  	unsigned long value=(gp2x_memregs[0x1198>>1] & 0x00FF);
  	unsigned long res;
  	if(value==0xFD) value=0xFA;
  	if(value==0xF7) value=0xEB;
  	if(value==0xDF) value=0xAF;
  	if(value==0x7F) value=0xBE;
  	res=~((gp2x_memregs[0x1184>>1] & 0xFF00) | value | (gp2x_memregs[0x1186>>1] << 16));
  	if (!(gp2x_rotate&1)) {
  		if ( (res & GP2X_VOL_UP) &&  (res & GP2X_VOL_DOWN)) gp2x_sound_volume(100,100);
  		if ( (res & GP2X_VOL_UP) && !(res & GP2X_VOL_DOWN)) gp2x_sound_volume(sound_vol+1,sound_vol+1);
  		if (!(res & GP2X_VOL_UP) &&  (res & GP2X_VOL_DOWN)) gp2x_sound_volume(sound_vol-1,sound_vol-1);
  	}
  	return res;
*/
}

void gp2x_sound_volume(int L /*0..100*/, int R /*0..100*/)
{
 L=(((L*0x50)/100)<<8)|((R*0x50)/100);          
 ioctl(gp2x_dev[4], SOUND_MIXER_WRITE_PCM, &L); 
}

/*
void gp2x_sound_volume(int l, int r)
{
 	l=l<0?0:l; l=l>319?319:l; r=r<0?0:r; r=r>319?319:r;
 	sound_vol=l;
 	l=(((l*0x50)/100)<<8)|((r*0x50)/100); /*0x5A, 0x60*/
 	//ioctl(gp2x_dev[2], SOUND_MIXER_WRITE_PCM, &l); /*SOUND_MIXER_WRITE_VOLUME*/
//}


void gp2x_timer_delay(unsigned long ticks)
{
	unsigned long ini=gp2x_timer_read();
	while (gp2x_timer_read()-ini<ticks);
}


unsigned long gp2x_timer_read(void)
{
 	return gp2x_memregl[0x0A00>>2]/gp2x_ticks_per_second;
}


void gp2x_sound_play(void *buff, int len)
{
	write(gp2x_dev[1], buff, len);	
}


void gp2x_init(int ticks_per_second, int bpp, int rate, int bits, int stereo, int Hz)
{
  if(!gp2x_dev[2])  gp2x_dev[2] = open("/dev/mem",   O_RDWR); 
  //if(!gp2x_dev[3])  gp2x_dev[3] = open("/dev/dsp",   O_WRONLY);
  if(!gp2x_dev[4])  gp2x_dev[4] = open("/dev/mixer", O_RDWR);
  //gp2x_dev[0] = open("/dev/mem",   O_RDWR); 
  //gp2x_dev[1] = open("/dev/dsp",   O_WRONLY);
  //gp2x_dev[2] = open("/dev/mixer", O_RDWR);

  gp2x_memregl=(unsigned long  *)mmap(0, 0x10000,                    PROT_READ|PROT_WRITE, MAP_SHARED, gp2x_dev[2], 0xc0000000);
  gp2x_memregs=(unsigned short *)gp2x_memregl;
	cpuctrl_init(); /* cpuctrl.c */
	Disable_940(); /* cpuctrl.c */



	//atexit(gp2x_deinit);
}


extern int fcloseall(void);

void gp2x_deinit(void)
{
	cpuctrl_deinit(); /* cpuctrl.c */
	gp2x_memregs[0x28DA>>1]=0x4AB; /*set video mode*/
	gp2x_memregs[0x290C>>1]=640;   
  	munmap((void *)gp2x_dualcore_ram, gp2x_dualcore_ram_size);
	munmap((void *)gp2x_2dregs, 0x100);
	//munmap((void *)gp2x_logvram15[1],640*480);
	close(gp2x_dev[2]);
	close(gp2x_dev[4]);
	//munmap((void *)gp2x_logvram15[0],640*480);
  	munmap((void *)gp2x_memregl, 0x10000);
 	//{ int i; for(i=0;i<5;i++) if(gp2x_dev[i]) close(gp2x_dev[i]); } /*close all devices*/

	fcloseall(); /*close all files*/
  	//chdir("/usr/gp2x"); /*go to menu*/
  	//execl("gp2xmenu","gp2xmenu",NULL);
}

void SetGP2XClock(int mhz)
{
	set_display_clock_div(16+8*(mhz>=220));
	set_FCLK(mhz);
	set_DCLK_Div(0);
	set_920_Div(0);
}

void SetVideoScaling(int pixels,int width,int height)
{
	float x, y;
	float xscale,yscale;

	int bpp=(gp2x_memregs[0x28DA>>1]>>9)&0x3;  /* bytes per pixel */

	if(gp2x_memregs[0x2800>>1]&0x100) /* TV-Out ON? */
	{
		xscale=489.0; /* X-Scale with TV-Out (PAL or NTSC) */
		if (gp2x_memregs[0x2818>>1]  == 287) /* PAL? */
			yscale=(pixels*274.0)/320.0; /* Y-Scale with PAL */
		else if (gp2x_memregs[0x2818>>1]  == 239) /* NTSC? */
			yscale=(pixels*331.0)/320.0; /* Y-Scale with NTSC */
	}
  	else /* TV-Out OFF? */
  	{
		xscale=1024.0; /* X-Scale for LCD */
		yscale=pixels; /* Y-Scale for LCD */
	}

	x=(xscale*width)/320.0;
	y=(yscale*bpp*height)/240.0;
	gp2x_memregs[0x2906>>1]=(unsigned short)x; /* scale horizontal */
	gp2x_memregl[0x2908>>2]=(unsigned  long)y; /* scale vertical */
	gp2x_memregs[0x290C>>1]=pixels*bpp; /* Set Video Width */
}

void gp2x_malloc_init(void) {
	gp2x_extended_ram = gp2x_dualcore_ram;
}

static __inline void *gp2x_malloc_extended_ram(unsigned long size) {
	void *ptr=NULL;
	ptr=gp2x_extended_ram;
	if ( ((unsigned long)ptr) >= ((unsigned long)gp2x_dualcore_ram+gp2x_dualcore_ram_size) )
		return (NULL);
	gp2x_extended_ram+=size;
	if ( ((unsigned long)gp2x_extended_ram) > ((unsigned long)gp2x_dualcore_ram+gp2x_dualcore_ram_size) )
		return (NULL);
	return (ptr);
}

void *gp2x_malloc(unsigned long size) {
	void *ptr=NULL;
	if (size>=(8*1024*1024)) {
		ptr=gp2x_malloc_extended_ram(size);
		if (ptr)
			return (ptr);
	}
	ptr=malloc(size);
	if (ptr)
		return (ptr);
	ptr=gp2x_malloc_extended_ram(size);
	if (ptr)
		return (ptr);
	return (NULL);
}

void gp2x_free(void *ptr) {

	if (( ((unsigned long)ptr) >= ((unsigned long)gp2x_dualcore_ram) ) &&
		( ((unsigned long)ptr) < ((unsigned long)gp2x_dualcore_ram+gp2x_dualcore_ram_size )) )
		return;
	free(ptr);
}
