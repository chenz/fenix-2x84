#include "gp2xcompat.h"

#include <string.h>

#define _X(name) #name,
static const char* input_names[XC_NUM_INPUTS] = {
	_XC_INPUTS
};
#undef _X

#define _X(name) SDLK_UNKNOWN,
SDLKey xc_keys[XC_NUM_INPUTS] = {
	_XC_INPUTS
};
#undef _X

typedef struct {
	SDLKey key;
	const char* name;
} KEY_NAME;

#define _X(name) {SDLK_ ## name, #name}
static const KEY_NAME key_names[] = {
	_X(BACKSPACE),
	_X(TAB),
	_X(CLEAR),
	_X(RETURN),
	_X(PAUSE),
	_X(ESCAPE),
	_X(SPACE),
	_X(EXCLAIM),
	_X(QUOTEDBL),
	_X(HASH),
	_X(DOLLAR),
	_X(AMPERSAND),
	_X(QUOTE),
	_X(LEFTPAREN),
	_X(RIGHTPAREN),
	_X(ASTERISK),
	_X(PLUS),
	_X(COMMA),
	_X(MINUS),
	_X(PERIOD),
	_X(SLASH),
	_X(0),
	_X(1),
	_X(2),
	_X(3),
	_X(4),
	_X(5),
	_X(6),
	_X(7),
	_X(8),
	_X(9),
	_X(COLON),
	_X(SEMICOLON),
	_X(LESS),
	_X(EQUALS),
	_X(GREATER),
	_X(QUESTION),
	_X(AT),
	_X(LEFTBRACKET),
	_X(BACKSLASH),
	_X(RIGHTBRACKET),
	_X(CARET),
	_X(UNDERSCORE),
	_X(BACKQUOTE),
	_X(a),
	_X(b),
	_X(c),
	_X(d),
	_X(e),
	_X(f),
	_X(g),
	_X(h),
	_X(i),
	_X(j),
	_X(k),
	_X(l),
	_X(m),
	_X(n),
	_X(o),
	_X(p),
	_X(q),
	_X(r),
	_X(s),
	_X(t),
	_X(u),
	_X(v),
	_X(w),
	_X(x),
	_X(y),
	_X(z),
	_X(DELETE),
	_X(KP0),
	_X(KP1),
	_X(KP2),
	_X(KP3),
	_X(KP4),
	_X(KP5),
	_X(KP6),
	_X(KP7),
	_X(KP8),
	_X(KP9),
	_X(KP_PERIOD),
	_X(KP_DIVIDE),
	_X(KP_MULTIPLY),
	_X(KP_MINUS),
	_X(KP_PLUS),
	_X(KP_ENTER),
	_X(KP_EQUALS),
	_X(UP),
	_X(DOWN),
	_X(RIGHT),
	_X(LEFT),
	_X(INSERT),
	_X(HOME),
	_X(END),
	_X(PAGEUP),
	_X(PAGEDOWN),
	_X(F1),
	_X(F2),
	_X(F3),
	_X(F4),
	_X(F5),
	_X(F6),
	_X(F7),
	_X(F8),
	_X(F9),
	_X(F10),
	_X(F11),
	_X(F12),
	_X(F13),
	_X(F14),
	_X(F15),
	_X(NUMLOCK),
	_X(CAPSLOCK),
	_X(SCROLLOCK),
	_X(RSHIFT),
	_X(LSHIFT),
	_X(RCTRL),
	_X(LCTRL),
	_X(RALT),
	_X(LALT),
	_X(RMETA),
	_X(LMETA),
	_X(LSUPER),
	_X(RSUPER),
	_X(MODE),
	_X(COMPOSE),
	_X(HELP),
	_X(PRINT),
	_X(SYSREQ),
	_X(BREAK),
	_X(MENU),
	_X(POWER),
	_X(EURO),
	_X(UNDO),
	{SDLK_LAST, "NONE"},
	{SDLK_UNKNOWN, NULL}
};
#undef _X

SDLKey xc_map_key(const int key)
{
	switch (key) {
	case 72:
		return xc_keys[XC_UP];
	case 80:
		return xc_keys[XC_DOWN];
	case 75:
		return xc_keys[XC_LEFT];
	case 77:
		return xc_keys[XC_RIGHT];
	case 29: // CTRL?
		return xc_keys[XC_A];
	case 56: // ALT?
		return xc_keys[XC_B];
	case 28:
		return xc_keys[XC_START];
	case 57:
		return xc_keys[XC_SELECT];
	case 15:
		return xc_keys[XC_R];
	case 14:
		return xc_keys[XC_L];
	case 30:
	case 31:
		// no mapping for volume keys
		return SDLK_LAST;
	case 46:
		return xc_keys[XC_PUSH];
	case 44:
		return xc_keys[XC_Y];
	case 45:
		return xc_keys[XC_X];
	default:
		return SDLK_UNKNOWN;
	}
}

CONFIG_RESULT xc_config_handler(
	const char* section,
	const char* name,
	const char* value,
	char* error, int error_size)
{
	if (!strcasecmp(section, "keys")) {
		for (int input = 0; input  < XC_NUM_INPUTS; input++) {
			if (!strcasecmp(name, input_names[input])) {
				for (const KEY_NAME* kn = key_names; kn->name; kn++) {
					if (!strcasecmp(value, kn->name)) {
						xc_keys[input] = kn->key;
						return CONFIG_OK;
					}
				}
				snprintf(error, error_size, "unknown key name: '%s'\n", value);
				return CONFIG_ERROR;
			}
		}
		return CONFIG_UNKNOWN_NAME;
	}
	else {
		return CONFIG_UNKNOWN_SECTION;
	}
}

