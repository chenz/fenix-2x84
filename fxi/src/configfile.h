#ifndef CONFIGFILE_H
#define CONFIGFILE_H

typedef enum {
	CONFIG_OK,
	CONFIG_UNKNOWN_SECTION,
	CONFIG_UNKNOWN_NAME,
	CONFIG_ERROR,
} CONFIG_RESULT;

typedef CONFIG_RESULT (*CONFIG_HANDLER)(
		const char* section,
		const char* name,
		const char* value,
		char* error,
		int error_size);

int configfile_parse(const char* filename, CONFIG_HANDLER* handlers);

#endif /* CONFIGFILE_H */
