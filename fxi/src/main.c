/*
 *  Fenix - Videogame compiler/interpreter
 *  Current release       : FENIX - PROJECT 1.0 - R 0.84
 *  Last stable release   :
 *  Project documentation : http://fenix.divsite.net
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 *  Copyright � 1999 Jos� Luis Cebri�n Pag�e
 *  Copyright � 2002 Fenix Team
 *
 */

/*
 * FILE        : main.c
 * DESCRIPTION : Main entry point for FXI
 *
 * HISTORY:	0.81 - Removed -w option
 *
 */

/*
 * INCLUDES
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <SDL.h>
#include "fxi.h"
#include "font.h"
#include "dcb.h"
#include "configfile.h"

#if defined(TARGET_GP2X)
// **** GP2X
#include <SDL_image.h>
#include "minimal.h"
#include "image.h"
#include "carfenix.h"
#endif

#if defined(USE_GP2X_COMPAT)
#include "gp2xcompat.h"
#endif

#if defined(TARGET_linux)
#include <libgen.h>
#endif

extern int enable_2xscale;
extern int enable_opengl;
extern int gl_scale;
extern int enable_console;
extern int enable_profiler;

void RectFillDraw (SDL_Surface* screen, int x1, int y1, int x2, int y2, Uint8 R, Uint8 G, Uint8 B) {
  Uint32 color = SDL_MapRGB(screen->format, R, G, B);
  SDL_Rect srcRect;
  srcRect.x = x1;
  srcRect.y = y1;
  srcRect.w = x2-x1;
  srcRect.h = y2-y1;
  SDL_FillRect(screen, &srcRect, color);
}

void drawSprite(SDL_Surface* imageSurface, SDL_Surface* screenSurface, int srcX, int srcY, int dstX, int dstY, int width, int height)
{
  SDL_Rect srcRect;
  srcRect.x = srcX;
  srcRect.y = srcY;
  srcRect.w = width;
  srcRect.h = height;
  
  SDL_Rect dstRect;
  dstRect.x = dstX;
  dstRect.y = dstY;
  dstRect.w = width;
  dstRect.h = height;
  
  SDL_BlitSurface(imageSurface, &srcRect, screenSurface, &dstRect);
}

void drawText (SDL_Surface* imageSurface, SDL_Surface* screenSurface, int x, int y, char * texto) {
  int i=0;
  while (texto[i] != 0) {
    if (texto[i] == 32) {
      RectFillDraw (screenSurface, x, y, x+7, y+8, 0, 0, 0);
      x += 7;
    }
    else {
      drawSprite(imageSurface, screenSurface, (texto[i]-46)*7, 0, x, y, 7, 8);
      x += 7;
    }
    i++;
  }
}
// *****

/*
 *  GLOBAL VARIABLES
 */

int debug = 0 ;		/* EXTERN defined in fxi.h */
int fxi   = 1 ;		/* EXTERN defined in fxi.h */
int volume;
int gimme_vol_buttons;

#ifdef TARGET_MAC
    int current_file = 0;
    char files[][256];
#endif

extern int full_screen, double_buffer ;
char *apptitle;

static int is_true(const char *s)
{
	return
		!strcasecmp(s, "true") ||
		!strcasecmp(s, "yes") ||
		!strcasecmp(s, "y") ||
		!strcasecmp(s, "1");
}

static CONFIG_RESULT config_handler(
	const char* section,
	const char* name,
	const char* value,
	char* error,
	int error_size)
{
	(void) error;
	(void) error_size;
	if (!strcasecmp(section, "options")) {
		if (!strcasecmp(name, "debugger")) {
			debug = is_true(value);
		}
		else if (!strcasecmp(name, "filtering")) {
			enable_filtering = is_true(value);
		}
		else if (!strcasecmp(name, "scale2x")) {
			enable_2xscale = is_true(value);
		}
		else if (!strcasecmp(name, "opengl")) {
			enable_opengl = is_true(value);
		}
		else if (!strcasecmp(name, "scale")) {
			gl_scale = atoi(value);
		}
		else if (!strcasecmp(name, "console")) {
			enable_console = is_true(value);
		}
		else if (!strcasecmp(name, "profiler")) {
			enable_profiler = is_true(value);
		}
		else if (!strcasecmp(name, "fullscreen")) {
			full_screen = is_true(value);
		}
		else if (!strcasecmp(name, "window")) {
			full_screen = !is_true(value);
		}
		else {
			return CONFIG_UNKNOWN_NAME;
		}
		return CONFIG_OK;
	}
	return CONFIG_UNKNOWN_SECTION;
}

static CONFIG_HANDLER config_handlers[] = {
	config_handler,
#if defined(USE_GP2X_COMPAT)
	xc_config_handler,
#endif
	NULL
};

/*
 *  FUNCTION : do_exit
 *
 *  Exits from the program cleanly ending operations
 *
 *  PARAMS:
 *      INT n: ERROR LEVEL to return to OS
 *
 *  RETURN VALUE: 
 *      No value
 *
 */

void
do_exit(int n)
{
	if (keytab_initialized) keytab_free() ;	
	if (selected_joystick!=NULL) SDL_JoystickClose(selected_joystick) ;
	SDL_Quit() ;
	#if defined(TARGET_GP2X)
	SetGP2XClock(200);
	gp2x_deinit();
	#endif
	exit(n) ;
}

void usage(const char* argv0, FILE* fp)
{
	char buf[4000];
	snprintf(
		buf,
		sizeof(buf),
		VERSION "\nCopyright(C) 2002 Fenix Team\nCopyright (C)1999 Jose Luis Cebrian\n"
		"Fenix comes with ABSOLUTELY NO WARRANTY; see COPYING for details\n\n"
		"Usage: %s [options] file.dcb\n\n"
		"   -h|--help    Show help\n"
#ifdef USE_DEBUGGER
		"   -d           Enable debugger\n"
#endif
//		"   -w           Execute in WINDOWED mode\n"
		"   -f           16bpp Filter ON (only 16bpp color mode)\n"
#ifdef USE_OPENGL
		"                Does not work with `--opengl`.\n"
#endif
		"   -i <DIR>     Add DIR to search path for opening files\n"
//		"   -b           Double buffer ON\n\n"
		"   --scale2x    Turn on graphics scaling with Scale2X algorithm\n"
#ifdef USE_OPENGL
		"                Does not work with `--opengl`.\n"
#endif
#ifdef USE_OPENGL
		"   --opengl     Use OpenGL for display\n"
		"   --scale [N]  Scale factor (default: %d)\n"
		"                Only works with `--opengl`\n"
#endif
		"   --console    Enable console\n"
		"   --profiler   Enable profiler\n"
		"   --fullscreen Fullscreen mode\n"
		"   --window     Window mode (default)\n"
		"\n"
		"This program is free software dsitributed under.\n\n"
		"GNU General Public License published by Free Software Foundation.\n"
		"Permission granted to distribute and/or modify as stated in the license\n"
		"agreement (GNU GPL version 2 or later).\n"
		"See COPYING for license details.\n",
		argv0, DEFAULT_GL_SCALE);

	if (fp) {
		fprintf(fp, "%s\n", buf);
	}
	else {
		gr_error(buf);
	}
}

/*
 *  FUNCTION : main
 *
 *  Main function for FXI
 *
 *  PARAMS:
 *      INT n: ERROR LEVEL to return to OS
 *
 *  RETURN VALUE: 
 *      No value
 *
 */

int main (int argc, char **argv)
{
	const char * filename = 0 ;
	char dcbname[256] ;
	INSTANCE * mainproc_running ;
	int i ;
	const SDL_version * sdl_version ;
	#if defined(TARGET_GP2X)
	// ******** GP2X **************
	FILE *pf = NULL;
	char config[20];
	char fichero[256];
	int speed = 200;
	int splash = 1;
	int mode = 0; // GP32
	int done = 0;
	// ****************************
	#endif
	int opt_debug = -1;
	int opt_enable_filtering = -1;
	int opt_enable_2xscale = -1;
	int opt_enable_opengl = -1;
	int opt_scale = -1;
	int opt_enable_console = -1;
	int opt_enable_profiler = -1;
	int opt_full_screen = -1;

	gr_con_printf ("�15" VERSION) ;

	#ifdef WIN32
	#define USAGE_STDOUT NULL
	#define USAGE_STDERR NULL
	#else
	#define USAGE_STDOUT stdout
	#define USAGE_STDERR stderr
	#endif

	/* Parse command line arguments */
	for (i = 1 ; i < argc ; i++) {
		const char* arg = argv[i];
		const char* next_arg = (i + 1 < argc) ? argv[i + 1] : NULL;

#define CHECK_NEXT_ARG													\
		do {															\
			if (!next_arg || *next_arg == '-') {						\
				gr_error("ERROR: argument '%s' requires a parameter.", arg); \
				return 1;												\
			}															\
			i++;														\
		} while (0)

		if (!strcmp(arg, "-h") || !strcmp(arg, "--help")) {
			usage(argv[0], USAGE_STDOUT);
			return 0;
		}
		#ifdef USE_DEBUGGER
		else if (!strcmp(arg, "-d")) {
			opt_debug = 1;
		}
		#endif
		else if (!strcmp(arg, "-f")) {
			opt_enable_filtering = 1;
		}
		else if (!strcmp(arg, "-i")) {
			CHECK_NEXT_ARG;
			file_addp(next_arg);
		}
		else if (!strcmp(arg, "--scale2x")) {
			opt_enable_2xscale = 1;
		}
		#ifdef USE_OPENGL
		else if (!strcmp(arg, "--opengl")) {
			opt_enable_opengl = 1;
		}
		else if (!strcmp(arg, "--scale")) {
			CHECK_NEXT_ARG;
			opt_scale = atoi(next_arg);
			if (opt_scale < 1) {
				gr_error("ERROR: invald scale: '%s'", next_arg);
				return 1;
			}
		}
		#endif
		else if (!strcmp(arg, "--console")) {
			opt_enable_console = 1;
		}
		else if (!strcmp(arg, "--profiler")) {
			opt_enable_profiler = 1;
		}
		else if (!strcmp(arg, "--fullscreen")) {
			opt_full_screen = 1;
		}
		else if (!strcmp(arg, "--window")) {
			opt_full_screen = 0;
		}
		else if (*arg == '-') {
			gr_error("ERROR: unknown argument: '%s'.", arg);
			return 1;
		}
		else if (!filename) {
			filename = arg;
		}
		else {
			gr_error("ERROR: a filename was already specified.");
			return 1;
		}
	}

	if (!filename) {
		usage(argv[0], USAGE_STDERR);
		return 1;
	}

	// Find and read configuration files
	#define CONFIG_FILENAME "fxi-2x84.conf"
	#if defined(TARGET_linux)
	{
		char buf[256];
		char filename_cpy[256];
		snprintf(buf, sizeof(buf), "%s/.config/%s", getenv("HOME"), CONFIG_FILENAME);
		if (file_exists(buf) && !configfile_parse(buf, config_handlers)) {
			return 1;
		}
		snprintf(filename_cpy, sizeof(filename_cpy), "%s", filename);
		snprintf(buf, sizeof(buf), "%s/%s", dirname(filename_cpy), CONFIG_FILENAME);
		if (file_exists(buf) && !configfile_parse(buf, config_handlers)) {
			return 1;
		}
	}
	#endif

	// Now apply the command line options so they will override any
	// values set in the config files.
	if (opt_debug != -1)
		debug = opt_debug;
	if (opt_enable_filtering != -1)
		enable_filtering = opt_enable_filtering;
	if (opt_enable_2xscale != -1)
		enable_2xscale = opt_enable_2xscale;
	if (opt_enable_opengl != -1)
		enable_opengl = opt_enable_opengl;
	if (opt_scale != -1)
		gl_scale = opt_scale;
	if (opt_enable_console != -1)
		enable_console = opt_enable_console;
	if (opt_enable_profiler != -1)
		enable_profiler = opt_enable_profiler;
	if (opt_full_screen != -1)
		full_screen = opt_full_screen;

	if (enable_opengl) {
		if (enable_filtering) {
			gr_error("ERROR: OpenGL cannot be used together with filtering");
			return 1;
		}
		if (enable_2xscale) {
			gr_error("ERROR: OpenGL cannot be used together with Scale2x");
			return 1;
		}
	}

	if (gl_scale < 1 || gl_scale > 20) {
		gr_error("ERROR: scale value is out of bounds: %d", gl_scale);
		return 1;
	}

	#if defined(TARGET_GP2X)
	#error GP2X option parsing no longer works...
	// ******* GP2X Overclock *********
	for (i = 1 ; i < argc ; i++) {
	  if (argv[i][0] == '-') {
	    j = 1 ;
	    while (argv[i][j]) {
	      if (argv[i][j] == 's') {
		speed = (argv[i][j+1]-48)*100;
		speed +=  (argv[i][j+2]-48)*10;
		speed +=  argv[i][j+3]-48;
	      }
	      if (argv[i][j] == 'n' && argv[i][j+2] == 's' && argv[i][j+7] == 'h') splash = 0; // No Splash screen
	      if (argv[i][j] == 'g' && argv[i][j+1] == 'p' && argv[i][j+2] == '2' && argv[i][j+3] == 'x') mode = 1; // GP2X
	      j++;
	    }
	  }
	}
	if (speed > 300 || speed < 100) speed = 200;
	#endif // TARGET_GP2X
	// *********************
 
	/* Init RAND generator */
	srand (time(NULL)) ;

	#if defined(TARGET_GP2X)
	gp2x_init(1000, 16, 11025,16,1,60);
	SetGP2XClock(speed);
	#endif

	/* Init SDL info */
	if ( SDL_Init (SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_NOPARACHUTE) < 0 ) {
		printf ("SDL Init Error: %s\n", SDL_GetError()) ;
		do_exit(1) ;
	}

	#if defined(TARGET_GP2X)
	// *********** GP2X *************
	selected_joystick = SDL_JoystickOpen(0);
	gimme_vol_buttons = mode;
	volume = 100;

	gp2x_sound_volume (volume, volume);

	if (splash) {
	  SDL_ShowCursor(0);
	  SDL_Surface* screen;
	  SDL_Surface* tmp;
	  SDL_Surface* bitmap;
	  int encontrado = 0;
	  int index = 0;
	  screen = SDL_SetVideoMode(320, 240, 16, SDL_SWSURFACE);
	  //SDL_Surface* bitmap = SDL_LoadBMP("image.bmp");
	  //SDL_Surface* font = SDL_LoadBMP("carfenix.bmp");

	  while (!encontrado) {
	    strcpy (fichero, argv[1]);
	    if (index == 0) {
	      fichero[strlen(argv[1])-3] = 'b';
	      fichero[strlen(argv[1])-2] = 'm';
	      fichero[strlen(argv[1])-1] = 'p';
	    }
	    else if (index == 1) {
	      fichero[strlen(argv[1])-3] = 'p';
	      fichero[strlen(argv[1])-2] = 'n';
	      fichero[strlen(argv[1])-1] = 'g';
	    }
	    else if (index == 2) {
	      fichero[strlen(argv[1])-3] = 'g';
	      fichero[strlen(argv[1])-2] = 'i';
	      fichero[strlen(argv[1])-1] = 'f';
	    }
	    else if (index == 3) {
	      fichero[strlen(argv[1])-3] = 'j';
	      fichero[strlen(argv[1])-2] = 'p';
	      fichero[strlen(argv[1])-1] = 'g';
	    }
	    else {
	      tmp = IMG_Load_RW(SDL_RWFromMem(&image_bmp, sizeof(image_bmp)),0);
	      bitmap=SDL_DisplayFormat(tmp);
	      SDL_FreeSurface(tmp);
	      encontrado = 1;
	    }
	    fichero[strlen(argv[1])] = 0;
	    if (((pf = fopen (fichero, "r")) != NULL) && !encontrado) {
	      bitmap = IMG_Load (fichero);
	      encontrado = 1;
	    }
	    index++;
	  }
	  tmp = IMG_Load_RW(SDL_RWFromMem(&carfenix_bmp, sizeof(carfenix_bmp)),0);
	  SDL_Surface * font=SDL_DisplayFormat(tmp);
	  SDL_FreeSurface(tmp);
	  
	  drawSprite(bitmap, screen, 0, 0, 0, 0, bitmap->w, bitmap->h);
	  
	  RectFillDraw (screen, 6, 188, 87, 234, 0, 0, 0);
	  drawText (font, screen, 8, 190, "FENIX  GP2X");
	  drawText (font, screen, 8, 198, "VER:  BETA4");
	  char velocidad[12];
	  sprintf (velocidad, "CPU: %dMHZ", speed);
	  velocidad[11] = 0;
	  drawText (font, screen, 8, 206, velocidad);
	  drawText (font, screen, 8, 216, "BY PUCK2099");
	  drawText (font, screen, 8, 224, "GP32WIP.COM");
	  
	  SDL_Flip(screen);

	  while (!done) {	    
	    SDL_Event event;      
	    SDL_WaitEvent(&event);      
	    switch (event.type) {
	    case SDL_JOYBUTTONDOWN:
	    case SDL_KEYDOWN:
	    case SDL_QUIT:
	      done = 1;
	      break;
	    }
	  }
	  if (splash) {
	    RectFillDraw (screen, 0, 0, 320, 240, 0, 0, 0);
	    SDL_Flip(screen);
	  }
	  SDL_FreeSurface (screen);
	}
	//*******************************
	#endif // TARGET_GP2X

	if (fxi) {
		/* we are calling FXI.EXE */
		sdl_version = SDL_Linked_Version();
		gr_con_printf ("�14SDL: %d.%d.%d (DLL loaded: %d.%d.%d)", 
				SDL_MAJOR_VERSION, SDL_MINOR_VERSION, SDL_PATCHLEVEL,
				sdl_version->major, sdl_version->minor, sdl_version->patch) ;
	}

	SDL_JoystickEventState (SDL_ENABLE) ;

	/* Initialization (modules needed before dcb_load) */

	gprof_init () ;
	string_init () ;
	init_c_type() ;
	
	/* Init application title for windowed modes */
	strcpy (dcbname, filename) ;
	apptitle = strdup(filename) ;

#ifdef TARGET_MAC
        strcpy (files[current_file], filename);
#endif


	if (!file_exists(dcbname))
	{
		/* Hack: con FXI renombrado, se permite que el fichero
		 * de datos tenga una extensi�n .DAT en vez de .DCB */

 		if (!fxi)
 		{
 			strcat (dcbname, ".dat") ;
 			if (!file_exists(dcbname))
 			{
 				strcpy (dcbname, filename) ;
 				strcat (dcbname, ".dcb") ;
 			}
 		}
 		else
 		{
 			strcat (dcbname, ".dcb") ;
 		}
	}

	if (!dcb_load (dcbname))
		return -1 ;

	if (!enable_profiler)
		gprof_toggle();

	/* Initialization (modules needed after dcb_load) */
	
	fnc_init();
	#ifdef MMX_FUNCTIONS
		MMX_init();
	#endif

	grlib_init () ;
	sysproc_init () ;
	gr_font_systemfont (default_font);

	if (argc > 32) argc = 32 ;
	for (i = 0 ; i < argc ; i++)
	{
		int * ptr = &GLODWORD(ARGV_TABLE+i*4) ;
		*ptr = string_new(argv[i]) ;
		string_use (*ptr) ;
	}
	GLODWORD(ARGC) = argc-1 ;

	if (mainproc)
	{
		mainproc_running = instance_new (mainproc, 0) ;
		instance_go_all () ;
	}

	//if (cd_playing()) cd_stop() ;
	//string_dump() ;
	//gprof_dump ("profile.txt");

	do_exit(0);
	return 0;
}

