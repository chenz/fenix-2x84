#include "configfile.h"

#include <SDL.h>
#include <files.h>
#include <grlib.h>

#include <inih/ini.h>

#include <stdio.h>

#define ERROR_SIZE 200

typedef struct
{
	const char* filename;
	CONFIG_HANDLER* handlers;
	char error[ERROR_SIZE];
} Context;

static int config_handler(
	void* user,
	const char* section,
	const char* name,
	const char* value)
{
	Context* context = (Context*) user;
	CONFIG_RESULT all_result = CONFIG_OK;
	for (CONFIG_HANDLER* handler = context->handlers;
		 *handler;
		 handler++) {
		CONFIG_RESULT result = (*handler)(section, name, value, context->error, ERROR_SIZE);
		if (result == CONFIG_OK) {
			return 1;
		}
		else if (result == CONFIG_ERROR) {
			return 0;
		}
		else if (all_result != CONFIG_UNKNOWN_NAME) {
			// CONFIG_UNKNOWN_NAME is already the most specific
			// condition
			all_result = result;
		}
	}
	if (all_result == CONFIG_UNKNOWN_SECTION) {
		fprintf(
			stderr,
			"WARNING: unknown section '%s' in file '%s'\n",
			section,
			context->filename);
	}
	else if (all_result == CONFIG_UNKNOWN_NAME) {
		fprintf(
			stderr,
			"WARNING: unknown setting '%s' in file '%s', section '%s'\n",
			name,
			context->filename,
			section);
	}
	return 1;
}

int configfile_parse(const char* filename, CONFIG_HANDLER* handlers)
{
	Context context;
 	context.filename = filename;
 	context.handlers = handlers;
	context.error[0] = 0;
	const int error = ini_parse(filename, config_handler, &context);
	if (error) {
		if (error < 0) {
			gr_error("ERROR: could not open file '%s'", filename);
		}
		else if (*context.error) {
			gr_error(
				"ERROR: in file '%s', line %d: %s",
				filename,
				error,
				context.error);
		}
		else {
			gr_error(
				"ERROR: in file '%s', line %d",
				filename,
				error);
		}
		return 0;
	}
	return 1;
}
