#include <stdio.h>
#include <stdlib.h>

#include "SDL.h"
#include "minimal.h"

void drawSprite(SDL_Surface* imageSurface, SDL_Surface* screenSurface, int srcX, int srcY, int dstX, int dstY, int width, int height)
{
  SDL_Rect srcRect;
  srcRect.x = srcX;
  srcRect.y = srcY;
  srcRect.w = width;
  srcRect.h = height;
  
  SDL_Rect dstRect;
  dstRect.x = dstX;
  dstRect.y = dstY;
  dstRect.w = width;
  dstRect.h = height;
  
  SDL_BlitSurface(imageSurface, &srcRect, screenSurface, &dstRect);
}

int main(int argc, char *argv[])
{
  SDL_Surface *screen;
  int done = 0;
  int fps = 0;
  int tiempo = SDL_GetTicks();

  gp2x_init(1000, 16, 11025,16,1,60);
	
  SetGP2XClock(245);

  if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) < 0)
    return 1;
  
  screen = SDL_SetVideoMode(320, 240, 16, SDL_SWSURFACE);
  if (!screen)
    {
      fprintf(stderr, "Couldn't set video mode: %s\n", SDL_GetError());
      return 1;
    }
  SDL_ShowCursor(SDL_DISABLE);
  
  SDL_Surface* bitmap = SDL_LoadBMP("image.bmp");
   
  drawSprite(bitmap, screen, 0, 0, 0, 0, bitmap->w, bitmap->h);
  
  SDL_Flip(screen);
  
  while (!done)
    {

      drawSprite(bitmap, screen, 0, 0, 0, 0, bitmap->w, bitmap->h);
  
      SDL_Flip(screen);

      SDL_Event event;
      
      while (SDL_PollEvent(&event)) {
      
      switch (event.type)
	{
	case SDL_JOYBUTTONDOWN:
	case SDL_KEYDOWN:
	case SDL_QUIT:
	  done = 1;
	  break;
	}
      }

      if (SDL_GetTicks() - tiempo > 1000) {
	tiempo = SDL_GetTicks();
	printf("%d\n", fps);
	fps = 0;
      }
      fps++;

    }
  SDL_Quit();
  return 0;
}
