#ifndef GP2XCOMPAT_H
#define GP2XCOMPAT_H

#include "configfile.h"

#include <SDL.h>

#define _XC_INPUTS \
	_X(UP) \
	_X(LEFT) \
	_X(DOWN) \
	_X(RIGHT) \
	_X(START) \
	_X(SELECT) \
	_X(L) \
	_X(R) \
	_X(A) \
	_X(B) \
	_X(X) \
	_X(Y) \
	_X(VOL_UP) \
	_X(VOL_DOWN) \
	_X(PUSH)

#define _X(code) XC_ ## code,
typedef enum {
	_XC_INPUTS
	XC_NUM_INPUTS
} XC_INPUT;
#undef _X

extern SDLKey xc_keys[XC_NUM_INPUTS];

SDLKey xc_map_key(const int key);

CONFIG_RESULT xc_config_handler(
	const char* section,
	const char* name,
	const char* value,
	char* error, int error_size);

#endif /* GP2XCOMPAT_H */
