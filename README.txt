fenix-2x84
==========

A version of the Fenix interpreter compatible with the one that was
used on the GP2X handheld. The purpose of this project is to be able
to interpret DCB files released for the GP2X on PCs.

The GP2X version was created by Puck2099. It was based on a snapshot
of the Fenix 0.84 source tree and required DCB file version 0x0200.

This version is based on copies of his source code found on
archive.org.

Status
------
- Linux-only
- 32-bit only (not easily fixable)
- Assumes Debian/Ubuntu-style multilib installation
- Mouse/touchpad support untested

TODO
----
- Better control of the display, scaling and fullscreen
- Support games that assume as case-insensitive file system

Changes
-------

Version 0.2
-----------
- OpenGL support
- Command line options can also be set in config file
- Fullscreen support
- Dedicated options for enabeling console and profiler
- Key bindings that worked only in "debug mode" before are now always enabled (see README.txt)
- ALT-F4 also works in fullscreen mode now

Version 0.1
-----------
- Take away control over some display options from the DCB program,
  let the user configure them instead
- Added configurable key-remapping
- Fixed some bugs
- Ported code to libpng 1.6 and zlib 1.2

Usage
-----
    Usage: fxi-2x84 [options] file.dcb

       -h|--help    Show help
       -d           Enable debugger
       -f           16bpp Filter ON (only 16bpp color mode)
                    Does not work with `--opengl`.
       -i <DIR>     Add DIR to search path for opening files
       --scale2x    Turn on graphics scaling with Scale2X algorithm
                    Does not work with `--opengl`.
       --opengl     Use OpenGL for display
       --scale [N]  Scale factor (default: 2)
                    Only works with `--opengl`
       --console    Enable console
       --profiler   Enable profiler
       --fullscreen Fullscreen mode
       --window     Window mode (default)

Some command line options will only be available if the corresponding
feature was compiled in.

Keys
----
- ALT-F4: Quit
- ALT-F: Toggle fullscreen
- ALT-G: Take a screenhot. Saved as shotNNNN.png in the current
  working directory. Does not overwrite existing files.

Console keys (only if console is enabled):
- ALT-C: Toggle console

Profiler keys (only if profiler is enabled):
- ALT-P: Toggle profiler view
- ALT-S: Toggle profiler updates
- ALT-R: Reset profiler

Configuration
-------------
The interpreter looks for a file called "fxi-2x84.conf" in
- $HOME/.config/
- the directory of the DCB file

Config settings
---------------
Config settings have to be placed in a `[options]` section. the
following settings are recognized:

- debugger (boolean)
- filtering (boolean)
- scale2x (boolean)
- opengl (boolean)
- scale (integer)
- console (boolean)
- profiler (boolean)
- fullscreen (boolean)
- window (boolean)

Key mapping
-----------
The default keymapping is as follows:

- UP, LEFT, DOWN, RIGHT: (arrow keys)
- START: return
- SELECT: space
- L: backspace
- R: tab
- A: lctrl
- B: lalt
- X: x
- Y: z
- VOL_UP: a
- VOL_DOWN: s
- PUSH: c

Note: `VOL_UP` and `VOL_DOWN` do nothing at the moment.

Key mapping can be configured via a `[keys]` section in the config
file. The mappings take the form `<GP2X INPUT NAME> = <KEY NAME>`.

The names listed above are the recognized input names.

The recognized key names are derived from the SDLKey enum in SDL 1.2,
with the prefix "SDLK_" removed. See `key_names` in
`fxi/src/gp2xcompat.c` for a complete list.

Here is an example key mapping configuration:

    [keys]
    up=up
    down=down
    left=left
    right=right
    select=space
    start=return
    a=a
    b=d
    x=s
    y=w
    l=1
    r=3
    push=p
    vol_up=none
    vol_down=none

Inputs that are not mapped in the config file will use the default
mapping. To leave an input unmapped, use key name `NONE`.

Building
--------
The build system assumes a Debian/Ubuntu style installation with the
following packages installed, along with the appropriate "*-dev"
packages.

- libsdl1.2debian:i386
- libsdl-mixer1.2:i386
- libsdl-image1.2:i386 
- libpng16-16:i386
- zlib1g:i386
- libgl1-mesa-glx:i386 (optional, required for OpenGL)

To build fxi-2x84, create a build config file in
`fxi/src/config.mk`. The minimal configuration looks like this (see
`fxi/src/Makefile` for further options):

    target=linux32

Then, run `make -C fxi/src` to build fxi-2x84.
