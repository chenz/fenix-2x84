/*
 *  Fenix - Videogame compiler/interpreter
 *  Current release       : FENIX - PROJECT 1.0 - R 0.84
 *  Last stable release   :
 *  Project documentation : http://fenix.divsite.net
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 *  Copyright � 1999 Jos� Luis Cebri�n Pag�e
 *  Copyright � 2002 Fenix Team
 *
 */

#ifdef ENGLISH
#define MSG_FILE_NOT_FOUND      "%s: file not found"
#define MSG_FILE_TOO_BIG        "%s: file too big"
#define MSG_FILE_EMPTY			"%s: file is empty"
#define MSG_READ_ERROR          "%s: file reading error"
#define MSG_DIRECTORY_MISSING   "You must specify a directory"
#define MSG_TOO_MANY_FILES      "Too many files specified"
#define MSG_USING               "Use: %s [options] filename\n\n"
#define MSG_OPTION_D            "   -d       Debugging mode\n"
#define MSG_OPTIONS             "   -i dir   Adds the directory to the PATH\n" \
                                "   -a       Automaticaly adds files to the DCB\n"  \
                                "   -f file  Adds a file to the DCB\n"  \
                                "   -g       Stores debugging information at the DCB\n"  \
                                "   -c       File uses the MS-DOS character set\n\n"  \
                                "This program is free software. You can distribute and/or modify it\n" \
                                "under the terms of the GNU General Public License as published by the\n" \
                                "Free Software Foundation; either the version 2 of the license or\n" \
                                "(at your will) any later version.\n\n" \
                                "Read the file COPYING for details\n\n"
#define MSG_COMPILE_ERROR       "Error in file %s at line %d: %s "
#define MSG_CONSTANT_NAME_EXP   "Constant name expected"
#define MSG_INVALID_IDENTIFIER  "Invalid identifier"
#define MSG_EXPECTED            "\"%s\" expected"
#define MSG_PROCNAME_EXP        "Procedure name expected"
#define MSG_INVALID_PARAM       "Parameter name invalid"
#define MSG_INVALID_PARAMT      "Parameter type invalid"
#define MSG_TOO_MANY_PARAMS     "Too many parameters in a definition"
#define MSG_INCORRECT_PARAMC    "Incorrect number of parameters"
#define MSG_NO_BEGIN            "BEGIN expected"
#define MSG_NO_END              "END expected"
#define MSG_ELSE_WOUT_IF        "ELSE without IF"
#define MSG_PROGRAM_NAME_EXP    "Program name expected"
#define MSG_PROCESS_NAME_EXP    "Procedure name expected"
#define MSG_INVALID_TYPE        "Invalid data type"
#define MSG_UNEXPECTED_TOKEN    "Unexpected token (too many ENDs?)"
#define MSG_NO_MAIN             "Main procedure was not defined"
#define MSG_INTEGER_REQUIRED    "Integer type required"
#define MSG_NUMBER_REQUIRED     "Numeric type required"
#define MSG_INCOMP_TYPES        "Data types not compatible with operation"
#define MSG_INCOMP_TYPE         "Data type not accepted here"
#define MSG_UNKNOWN_IDENTIFIER  "Unknown identifier"
#define MSG_NOT_AN_ARRAY        "Not an array or struct array"
#define MSG_BOUND               "Index out of range"
#define MSG_IDENTIFIER_EXP      "Identifier expected"
#define MSG_NOT_AN_LVALUE       "Can't get the address of an inmediate value"
#define MSG_NOT_A_POINTER       "Pointer required"
#define MSG_VARIABLE_REQUIRED   "Variable required"
#define MSG_STRUCT_REQUIRED     "Struct required"
#define MSG_DIVIDE_BY_ZERO      "Division by zero"
#define MSG_TYPES_NOT_THE_SAME  "Pointers are of incompatible type" 
#define MSG_CONSTANT_EXP        "Constant value expected"
#define MSG_STRING_EXP          "String expected"
#define MSG_NO_LOOP             "Out of loop"
#define MSG_INVALID_STEP        "Invalid STEP"
#define MSG_INVALID_SENTENCE    "Invalid sentence"
#define MSG_VTA                 "Can't create an array of undefined multiple sizes"
#define MSG_TOO_MANY_AL         "Too many array levels"
#define MSG_VARIABLE_ALREADY    "Variable already declared"
#define MSG_IDENTIFIER_EXP      "Identifier expected"
#define MSG_CANT_INIT_STR       "This struct can't be initialized"
#define MSG_TOO_MANY_INIT       "Too many initializers"
#define MSG_TOO_MANY_INCLUDES   "Too many nested includes"
#define MSG_IDENTIFIER_TOO_LONG "Identifier too long"
#define MSG_INVALID_CHAR        "Invalid Character"
#define MSG_TOO_MANY_TYPES      "Too many user-defined types"
#define MSG_UNDEFINED_PROC      "Undefined procedure"
#define MSG_NO_COMPATIBLE_DLL	"The library is not Fenix compatible"
#define MSG_TOO_MANY_SYSPROCS	"Too many system functions"
#define MSG_INCORRECT_PTR_INIT	"A pointer can only be initialized to 0"
#define MSG_NOT_ENOUGH_INIT		"Not enough initializers"
#define MSG_MULTIPLE_PROCS_FOUND "Various conflicting versions of %s found"
#define MSG_QUESTION_INC		"Incompatible types at the sides of ? operator"
#define MSG_UNKNOWN_PREP		"Unknown preprocessor directive"

#else

#define MSG_FILE_NOT_FOUND      "%s: fichero no encontrado"
#define MSG_FILE_TOO_BIG        "%s: fichero demasiado grande"
#define MSG_FILE_EMPTY			"%s: fichero vac�o"
#define MSG_READ_ERROR          "%s: error de lectura"
#define MSG_DIRECTORY_MISSING   "Debe especificar un directorio"
#define MSG_TOO_MANY_FILES      "Especifico demasiados ficheros"
#define MSG_USING               "Uso: %s [opciones] fichero\n\n"
#define MSG_OPTION_D            "   -d       Activar modo de depuracion\n"
#define MSG_OPTIONS             "   -i dir   Anade ese directorio al PATH\n" \
                                "   -a       Autoincluye ficheros en el DCB\n"  \
                                "   -f file  Incluye un fichero en el DCB\n"  \
                                "   -g       Guarda informacion de depurado\n"  \
                                "   -c       Usar caracteres MS-DOS\n\n"  \
                                "Este programa es software libre. Se permite distribuirlo y/o modificarlo\n" \
                                "segun los terminos de la GNU General Public License publicada por la\n" \
                                "Free Software Foundation; tanto la version 2 de la licencia como\n" \
                                "(a tu eleccion) cualquier version posterior\n\n" \
                                "Lee el fichero COPYING para obtener los detalles\n\n"
#define MSG_COMPILE_ERROR       "Error en linea %d: %s "
#define MSG_CONSTANT_NAME_EXP   "Se esperaba nombre de constante"
#define MSG_INVALID_IDENTIFIER  "Identificador invalido"
#define MSG_EXPECTED            "Se esperaba \"%s\""
#define MSG_PROCNAME_EXP        "Se esperaba nombre de proceso"
#define MSG_INVALID_PARAM       "Nombre de parametro incorrecto"
#define MSG_INVALID_PARAMT      "Tipo de parametro incorrecto"
#define MSG_TOO_MANY_PARAMS     "Demasiados parametros en definicion de proceso"
#define MSG_INCORRECT_PARAMC    "El numero de parametros es incorrecto"
#define MSG_NO_BEGIN            "Se esperaba BEGIN"
#define MSG_NO_END              "Se esperaba END"
#define MSG_ELSE_WOUT_IF        "ELSE sin IF"
#define MSG_PROGRAM_NAME_EXP    "Se esperaba nombre del programa"
#define MSG_PROCESS_NAME_EXP    "Se esperaba nombre del programa"
#define MSG_INVALID_TYPE        "Tipo de dato invalido"
#define MSG_UNEXPECTED_TOKEN    "Simbolo o instruccion inesperada aqui (�demasiados END?)"
#define MSG_NO_MAIN             "Proceso principal no definido"
#define MSG_INTEGER_REQUIRED    "Se requiere un dato entero"
#define MSG_NUMBER_REQUIRED     "Se requiere un dato numerico"
#define MSG_INCOMP_TYPES        "No se pueden operar esos tipos de dato entre si"
#define MSG_INCOMP_TYPE         "El tipo de dato no es valido ahi"
#define MSG_UNKNOWN_IDENTIFIER  "Identificador desconocido"
#define MSG_NOT_AN_ARRAY        "No es un array o estructura multiple"
#define MSG_BOUND               "�ndice fuera de rango"
#define MSG_IDENTIFIER_EXP      "Se esperaba identificador"
#define MSG_NOT_AN_LVALUE       "Solo se puede extraer la direccion de una variable"
#define MSG_NOT_A_POINTER       "Se requiere un puntero"
#define MSG_VARIABLE_REQUIRED   "Se requiere una variable"
#define MSG_STRUCT_REQUIRED     "Se requiere una estructura"
#define MSG_DIVIDE_BY_ZERO      "Division por cero"
#define MSG_TYPES_NOT_THE_SAME  "Los punteros no son del mismo tipo" 
#define MSG_CONSTANT_EXP        "Se esperaba un valor constante"
#define MSG_STRING_EXP          "Se esperaba una cadena"
#define MSG_NO_LOOP             "Fuera de bucle"
#define MSG_INVALID_STEP        "STEP invalido"
#define MSG_INVALID_SENTENCE    "Sentencia invalida"
#define MSG_VTA                 "No se puede crear un array de tamanos multiples sin especificarlos"
#define MSG_TOO_MANY_AL         "Demasiados niveles de array"
#define MSG_VARIABLE_ALREADY    "Variable ya declarada antes"
#define MSG_IDENTIFIER_EXP      "Se esperaba identificador"
#define MSG_CANT_INIT_STR       "No se puede inicializar esa estructura"
#define MSG_TOO_MANY_INIT       "Demasiados inicializadores"
#define MSG_TOO_MANY_INCLUDES   "Demasiados include anidados"
#define MSG_IDENTIFIER_TOO_LONG "Identificador demasiado largo"
#define MSG_INVALID_CHAR        "Caracter invalido"
#define MSG_TOO_MANY_TYPES      "Demasiados tipos definidos por el usuario"
#define MSG_UNDEFINED_PROC      "Procedimiento no definido"
#define MSG_NO_COMPATIBLE_DLL	"La libreria no es compatible Fenix"
#define MSG_TOO_MANY_SYSPROCS	"Demasiadas funciones del sistema"
#define MSG_INCORRECT_PTR_INIT	"Un puntero s�lo puede inicializarse a 0"
#define MSG_NOT_ENOUGH_INIT		"No hay suficientes inicializadores"
#define MSG_MULTIPLE_PROCS_FOUND "Hay disponibles varias versiones de %s en conflicto"
#define MSG_QUESTION_INC		"Tipos incompatibles a los lados del operador ?"
#define MSG_UNKNOWN_PREP		"Directiva de preprocesador desconocida"
#endif
