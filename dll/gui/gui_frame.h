/*
 *  Fenix - Videogame compiler/interpreter
 *  Current release       : FENIX - PROJECT 1.0 - R 0.9
 *  Last stable release   : 
 *  Project documentation : http://fenix.divsite.net
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software 
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 *  Copyright � 1999 Jos� Luis Cebri�n Pag�e
 *  Copyright � 2002 Fenix Team
 *
 */

/*
 * FILE        : gui_frame.h
 * DESCRIPTION : Graphic User Interface DLL header file
 */

#include "gui.h"

#ifndef __GUI_FRAME_H
#define __GUI_FRAME_H

/* ------------------------------------------------------------------------- * 
 *  FRAME
 * ------------------------------------------------------------------------- */

#define FRAME_RAISE		0x00
#define FRAME_LOWER		0x01
#define FRAME_LINE		0x02
#define FRAME_INSET		0x03

/** CONTROL:FRAME, a fixed text */

typedef struct _frame
{
	CONTROL		control;				/**< Parent class data */
	int 		type;					/**< Type of frame */
}
FRAME;

extern CONTROL * gui_frame_new (int width, int height, int type);

#endif
