<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE fenixdoc SYSTEM "fenixdoc.dtd">

<function name="LOAD_TTFAA">
<retval/>
<param name="fichero" type="STRING"> Nombre del fichero TTF </param>
<param name="tama�o"> Tama�o (altura en pixels) deseado </param>
<param name="profundidad"> Profundidad de color (debe ser 8 � 16) </param>
<param name="color"> Color escogido para el texto </param>
<param name="fondo"> Color escogido para el fondo </param>
<abstract> Recupera un tipo de letra Truetype del disco, creando una
    fuente interna a partir del tama�o deseado, con anti-alias
</abstract>
<description>
    <p>
	Esta funci�n recupera de disco un tipo de letra Truetype y crea
	internamente los bitmaps de todos los caracteres en el juego
	de caracteres est�ndar (ISO-8859-1). La funci�n devuelve un
	identificador de fuente (o tipo de letra) que puede utilizarse
	en las llamadas a funciones como <a href="WRITE"/> o
	<a href="WRITE_IN_MAP"/>.
    </p>
    <p>
	El tama�o indicado es un valor meramente orientativo. Existir�n
	caracteres cuyo tama�o final sea ligeramente superior al indicado.
	Adem�s, el tama�o final del caracter depende del tipo de letra,
	aunque a un mismo tama�o varios tipos de letra deben tener
	un aspecto comparable.
    </p>
    <p>
	Esta funci�n crea una fuente a color, utilizando el par�metro
	<i>color</i> para dibujar los caracteres. La profundidad de
	color puede ser de 8 � 16 bits, en funci�n del par�metro
	<i>profundidad</i>, y afecta a los bitmaps de todos los
	caracteres.
    </p>
    <p> 
	Tanto el par�metro "color" como el par�metro "fondo" indican
	colores cuyo significado depende del modo de v�deo actual.
	En modo de v�deo de 8 bits, los dos par�metros indicar�n un
	color de la paleta (de 0 a 255). En modo de 16 bits, son
	c�digos de color de 0 a 65535 cuyo significado var�a seg�n
	el ordenador. Se recomienda usar la funci�n <a href="RGB"/>
	para obtener ambos par�metros. Este modo de funcionamiento
	es equivalente al resto de funciones que requieren colores
	como par�metros, como por ejemplo <a href="TEXT_COLOR"/>
	o <a href="DRAWING_COLOR"/>.
    </p>
    <p>
	En ambos casos, los bordes de la fuente ser�n degradados
	mediante una t�cnica llamada "antialiasing", utilizando
	el color especificado en el par�metro <i>fondo</i>. Esta
	t�cnica mejora notablemente el aspecto de la fuente, siempre
	que se dibuje el texto encima de una superficie donde
	prevalezca el color indicado como fondo. Si no es el caso,
	puede usarse <a href="LOAD_TTF"/> para crear una fuente
	sin antialias, o crear varias versiones de la misma fuente
	usando varios colores de fondo distintos (con un coste
	importante en memoria).
    </p>
    <p>
	Si se desea, es posible guardar la fuente resultante con la
	funci�n SAVE_FNT. Un fichero FNT es mucho m�s r�pido de cargar
	que una fuente TTF, pero s�lo contiene la fuente final (los
	bitmaps de cada caracter), mientras el fichero TTF contiene
	informaci�n para dibujar texto a cualquier resoluci�n y tama�o.
    </p>
</description>
<note>
    <p>
	Tanto LOAD_TTF como LOAD_TTFA guardan informaci�n sobre el �ltimo
	fichero TTF recuperado en memoria, lo cual les permite acelerar
	las siguientes llamadas a la funci�n que se hagan con la misma
	funete (siempre que estas llamadas sean consecutivas). Esto
	puede ocurrir a menudo, por ejemplo si se desea utilizar un
	tipo de letra Truetype a varios tama�os. Sin embargo, se recomiendo
	usar ficheros TTF reducidos en tama�o, o al menos recuperar en
	�ltimo lugar el fichero TTF m�s peque�o.
    </p>
</note>
<see href="DLL/TTF/LOAD_TTF" name="LOAD_TTF"/>
<see href="WRITE"/>
<see href="WRITE_IN_MAP"/>
<see href="SAVE_FNT"/>
<see href="TEXT_COLOR"/>
</function>

